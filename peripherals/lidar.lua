function peripheral:setMode (char)
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 3, false)
    buffer:add_char(char)

    self:send(buffer)
end

function peripheral:getMode ()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 4, false)
    buffer:add_int8(0)

    local result = self:request(buffer)

    if result == nil then
        self:log("Mode request failed")
        return -1
    end

    local data = result:data()
    if (data[1] ~= 'c') and (data[1] ~= 'd') and (data[1] ~= 'w') and (data[1] ~= 's') then
        self:log("Invalid mode data")
        return -2
    end

    return data[1]
end


function peripheral:please_goto (x_relative, y_relative)

    -- x et y en float
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 5, false)
    buffer:add_float(x_relative)
    buffer:add_float(y_relative)

    self:send(buffer)

end

-- Renvoie un vecteur unitaire indiquant la direction relative au robot
function peripheral:next_direction ()

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 2, false)

    local res = self:request(buffer)
    if res == nil then
        return nil, nil
    end

    return data[1], data[2]
    
end
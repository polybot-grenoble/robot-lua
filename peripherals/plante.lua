function peripheral:open()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()

    buffer:set_destination(self.hermesID, 1, false)

    self:send(buffer)

end 

function peripheral:close()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()

    buffer:set_destination(self.hermesID, 2, false)

    self:send(buffer)
end

function peripheral:up()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()

    buffer:set_destination(self.hermesID, 3, false)
    
    self:send(buffer)
end

function peripheral:take()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()

    buffer:set_destination(self.hermesID, 4, false)
    self:send(buffer)
end

function peripheral:stop()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()

    buffer:set_destination(self.hermesID, 5, false)
    self:send(buffer)

end

function peripheral:initpos()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()

    buffer:set_destination(self.hermesID, 6, false)
    self:send(buffer)

end

function peripheral:down()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()

    buffer:set_destination(self.hermesID, 7, false)
    self:send(buffer)
end

function peripheral:open_slow()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()

    buffer:set_destination(self.hermesID, 8, false)
    self:send(buffer)
end
peripheral.robot_radius_mm = 400 -- To change

function peripheral:unplug_motors ()

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 0, false)
    buffer:add_int8(0)

    self:send(buffer)

end

function peripheral:stop ()
    
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 1, false)
    buffer:add_int8(0)

    self:send(buffer)

end

-- x, y, z : float
function peripheral:set_relative_speed (x, y, z)
    
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 2, false)
    buffer:add_float(x)
    buffer:add_float(y)
    buffer:add_float(z)

    self:send(buffer)

end

-- x, y, z : float
function peripheral:set_speed (x, y, z)
    
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 3, false)
    buffer:add_float(x)
    buffer:add_float(y)
    buffer:add_float(z)

    self:send(buffer)

end


function peripheral:is_stopped()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 12, false)

    local res = self:request(buffer, 0)

    if res == nil then
        self:log("is stopped response failed")
        return 1
    end

    local data = res:data()

    return data[1]
end

-- x, y, z : float
function peripheral:relative_goto (x, y, theta)
    
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 4, false)
    buffer:add_float(x)
    buffer:add_float(y)
    buffer:add_float(theta)

    self:send(buffer)

end

function peripheral:abs_goto (x, y, theta)

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 1, false)
    buffer:add_float(x)
    buffer:add_float(y)
    buffer:add_float(theta)

    local result = self:request(buffer, 4000)
    --self:send(buffer)

end

function peripheral:set_position (x, y, theta)
    
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 7, false)
    buffer:add_float(x)
    buffer:add_float(y)
    buffer:add_float(theta)

    self:send(buffer)

end

function peripheral:get_position ()
    
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 7, false)
    buffer:add_int8(0)

    local result = self:request(buffer)

    if result == nil then
        self:log("Team request failed")
        return -1
    end

    local data = result:data()

    return data

end

function peripheral:stop_with_response ()
    
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 11, false)
    buffer:add_int8(0)

    local result = self:request(buffer)

    if result == nil then
        self:log("stop response failed")
        return -1
    end
end
-- Tries its best to go to (x, y, theta)
-- Returns true if it reached its target
-- else it returns false
-- function peripheral:fake_goto (x, y, theta)

--     local pos = self:get_position()

--     if pos == nil then
--         return false
--     end

--     local dx = pos[0] - x -- mm
--     local dy = pos[1] - y -- mm
--     local dtheta = (pos[2] - theta + 3.14) % (2 * 3.14) -- rad

--     local distance = math.sqrt(dx * dx + dy * dy) -- mm

--     local speed = 100 -- mm/s
--     local travel_time = distance / speed -- s

--     local angular_speed = dtheta / travel_time  -- rad/s
--     local x_speed = (dx / distance) * speed     -- mm/s
--     local y_speed = (dy / distance) * speed     -- mm/s

--     local sleep_time = math.floor(1000 * travel_time)

--     self:log(
--         "[Fake GOTO] Calculated : "
--         .. "\n - (dx, dy, dtheta) = (" .. dx .. ", " .. dy .. ", " .. dtheta .. ")"
--         .. "\n - Distance = " .. distance
--         .. "\n - Travel time = " .. travel_time
--         .. "\n - Absolute speed = (" .. x_speed .. ", " .. y_speed .. "," .. angular_speed .. ")" 
--     )

--     self:set_speed(x_speed, y_speed, angular_speed)

--     self:sleep(sleep_time)

--     return true

-- end

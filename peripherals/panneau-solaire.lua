function peripheral:setTeam (char)
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 1, false)
    buffer:add_char(char)

    self:send(buffer)
end

function peripheral:getTeam ()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 2, false)
    buffer:add_int8(0)

    local result = self:request(buffer)

    if result == nil then
        self:log("Team request failed")
        return -1
    end

    local data = result:data()
    if (data[1] ~= 'S') and (data[1] ~= 'B') and (data[1] ~= 'Y') then
        self:log("Invalid team data")
        return -2
    end

    return data[1]
end

function peripheral:stop ()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 0, false)
    buffer:add_int8(0)

    self:send(buffer)
end
-- This peripheral goes with the script `samplePeripheral.py` in hermes-py
-- https://gitlab.com/polybot-grenoble/core/hermes-py/-/blob/master/samplePeripheral.py

function peripheral:hello (message)

    buf = HermesBuffer.new()
    buf:clear()
    buf:rewind()
    buf:set_destination(self.hermesID, 0, false)
    buf:add_string(message)

    self:send(buf)

end

function peripheral:goodbye (message)

    buf = HermesBuffer.new()
    buf:clear()
    buf:rewind()
    buf:set_destination(self.hermesID, 1, false)
    buf:add_string(message)
    
    self:send(buf)

end

function peripheral:read ()

    buf = HermesBuffer.new()
    buf:clear()
    buf:rewind()
    buf:set_destination(self.hermesID, 2, false)

    res = self:request(buf, 0)

    if res == nil then
        self:log("Failed to read")
        return nil
    end

    local data = res:data()

    return data[1]

end

function peripheral:show ()
    self:debug("X: " .. tostring(self.x))
end

peripheral.z = 32


function peripheral:make_test ()

    local b = HermesBuffer.new()
    b.command = 12
    self:send(b)

end

function peripheral:quit ()

    local b = HermesBuffer.new()
    b.command = 1
    self:send(b)

end

peripheral.handlers = {

    [12] = function (peripheral, buffer) 
        local data = buffer:data()
        local out = "Recieved wild buffer 12, contains: {\n"
        for k, v in ipairs(data) do
            out = out .. "  " .. tostring(k) .. ": " .. tostring(v) .. "\n\r"
        end
        out = out .. "}\n\r"
        peripheral:debug(out)
    end

}

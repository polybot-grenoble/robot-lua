-- Turning the red LED on
function peripheral:red_on ()
    -- We create a new buffer for the request
    local buffer = HermesBuffer.new()
    -- We clean it before using it
    buffer:clear()
    buffer:rewind()
    -- We set up the destination
    buffer:set_destination(self.hermesID, 1, false)
    -- We fill in the arguments
    buffer:add_int8(1)

    -- We send it without expecting a response
    self:send(buffer)
end

-- Turning the red LED off
function peripheral:red_off ()
    -- We create a new buffer for the request
	local buffer = HermesBuffer.new()
    -- We clean it before using it
	buffer:clear()
	buffer:rewind()
    -- We set up the destination
	buffer:set_destination(self.hermesID, 1, false)
    -- We fill in the arguments
	buffer:add_int8(0)

    -- We send it without expecting a response
	self:send(buffer)
end

-- Turning the red LED on
function peripheral:green_on ()
    -- We create a new buffer for the request
    local buffer = HermesBuffer.new()
    -- We clean it before using it
    buffer:clear()
    buffer:rewind()
    -- We set up the destination
    buffer:set_destination(self.hermesID, 2, false)
    -- We fill in the arguments
    buffer:add_int8(1)

    -- We send it without expecting a response
    self:send(buffer)
end

-- Turning the red LED off
function peripheral:green_off ()
    -- We create a new buffer for the request
	local buffer = HermesBuffer.new()
    -- We clean it before using it
	buffer:clear()
	buffer:rewind()
    -- We set up the destination
	buffer:set_destination(self.hermesID, 2, false)
    -- We fill in the arguments
	buffer:add_int8(0)

    -- We send it without expecting a response
	self:send(buffer)
end

-- Turning the red LED on
function peripheral:blue_on ()
    -- We create a new buffer for the request
    local buffer = HermesBuffer.new()
    -- We clean it before using it
    buffer:clear()
    buffer:rewind()
    -- We set up the destination
    buffer:set_destination(self.hermesID, 3, false)
    -- We fill in the arguments
    buffer:add_int8(1)

    -- We send it without expecting a response
    self:send(buffer)
end

-- Turning the red LED off
function peripheral:blue_off ()
    -- We create a new buffer for the request
	local buffer = HermesBuffer.new()
    -- We clean it before using it
	buffer:clear()
	buffer:rewind()
    -- We set up the destination
	buffer:set_destination(self.hermesID, 3, false)
    -- We fill in the arguments
	buffer:add_int8(0)

    -- We send it without expecting a response
	self:send(buffer)
end

-- Get the voltage read on the A0 pin
function peripheral:get_potar ()
    -- We create a new buffer for the request
    local buffer = HermesBuffer.new()
    -- We clean it before using it
    buffer:clear()
    buffer:rewind()
    -- We set up the destination
    buffer:set_destination(self.hermesID, 4, false)

    -- We send it expecting a response
    local result = self:request(buffer)

    -- We check that we received something (i.e. no timeout)
    if result == nil then
        self:log("Binary read request failed")
        return -1
    end

    -- We retreive the data 
    local data = result:data()
    -- We check if we have the right number of arguments returned
    if #data < 1 then
        self:log("Invalid binary read data")
        return -2
    end

    -- We return the reading
    return data[1]

end

function peripheral:refresh_coordinate()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 1, false)

    local result = self:request(buffer, 4000)

    if result == nil then
		self:error("No response from superviseur")
		return nil
	end

	local data = result:data()
	if #data < 2 then
		self:error("missing coordinates")
		return nil
	end

	return data[13], data[14], data[15]
end

function peripheral:plante_number()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 2, false)

    local result = self:request(buffer, 2000)

    if result == nil then
		self:error("No response from superviseur")
		return nil
	end

	local data = result:data()
	if #data < 2 then
		self:error("missing plants number")
		return nil
	end

	return {date[1], date[2], data[3], data[4], date[5], data[6], data[7], date[8], date[9], date[10],data[11], data[12]}

end


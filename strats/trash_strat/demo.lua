require "common_function"

strat.name = "Demo1" 
strat.team = 1              --TEAM BLEU 
strat.required_peripherals = {
    "plante", "pegasus", "panneau", "lidar"
}

cree_common (strat)


function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.plante:initpos()
end 

-- start in the private blue zone
function strat:main()

    self.pegasus:set_relative_speed(0,0,0.35) -- turns a little
    self:sleep(2000)
    
    self.pegasus:set_relative_speed(100,175,0) -- goes to first circle
    self:sleep(3500)
    self.pegasus:stop()

    self:get_plant()

    -- put them in the private blue zone
    self.pegasus:set_relative_speed(0,0,1.2) 
    self:sleep(2000) 


    self.pegasus:set_relative_speed(20,150,0)
    self:sleep(3500)
    self.pegasus:stop()

    self:set_plant()

    self.pegasus:set_relative_speed(0,0,1.2) -- turns at almost 68 deg 
    self:sleep(2000) 

    self.pegasus:set_relative_speed(80,300,0) -- 2nd circle
    self:sleep(5000)
    self.pegasus:stop()

    self:get_plant()

    self.pegasus:set_relative_speed(0,0,1.2) -- turns at almost 68 deg 
    self:sleep(2000) 

    self.pegasus:set_relative_speed(10,250,0)
    self:sleep(5000)
    self.pegasus:stop()

    self:set_plant()

    -- va au panneau
     
     self.pegasus:set_relative_speed(-320,0,0) 
     self:sleep(5000)
    

    for j = 1, 2 do
        for i = 1, 3 do
            self.panneau:setTeam("B")
    
            self.pegasus:set_relative_speed(0.1,-225,0) 
            self:sleep(1000)
        end
        self.pegasus:set_relative_speed(0.1,-300,0)
        self:sleep(1000)
    end

    self.pegasus:set_relative_speed(0.1,355,0)
    self:sleep(6000)
     
    self.pegasus:stop()
    self.panneau:stop()

end 

function strat:onEnd ()

    -- On s'assure que c'est éteint si la strat crash ou si elle 
    -- se fait timeout
    self.pegasus:stop()
    self.panneau:stop()
    self.plante:initpos()

end

require "common_function"
strat.name = "Test 4 Pegasus2" 
strat.team = 1
strat.required_peripherals = {
    "pegasus" 
}

--  if "plante" and "panneau" do not work
-- USE FUNCTION GOTO
cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.gamepad:updateData()
end 

-- has to go to the ending area
function strat:main()

    --FUNCTION GOTO 
    self.pegasus:stop()

end 

function strat:onEnd ()
    
    self.plante:stop()

end
require "common_function"
strat.name = "Test 5 Panneau pega" 
strat.team = 1
strat.required_peripherals = {
    "panneau", "pegasus"
}

cree_common (strat)


-- if "plante" does not work

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.panneau:initpos()
end 


function strat:main()

     -- va au panneau
     
     self.pegasus:set_relative_speed(-320,0,0) 
     self:sleep(5000)
    

    for j = 1, 2 do
        for i = 1, 3 do
            self.panneau:setTeam("B")
    
            self.pegasus:set_relative_speed(0.1,-225,0) 
            self:sleep(1000)
        end
        self.pegasus:set_relative_speed(0.1,-300,0)
        self:sleep(1000)
    end

    self.pegasus:set_relative_speed(0.1,355,0)
    self:sleep(6000)
     
    self.pegasus:stop()
    self.panneau:stop()

end 

function strat:onEnd ()
    self.pegasus:stop()
    self.panneau:stop()
    self.plante:initpos()
end
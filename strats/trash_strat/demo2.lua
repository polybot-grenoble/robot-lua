require "common_function"

strat.name = "Demo2" 
strat.team = 1          
strat.required_peripherals = {
    "pegasus", "plante", "panneau", "lidar"
}

--gardener test 
cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.plante:initpos()

end 

function strat:main()

      self.pegasus:set_relative_speed(0,0,0.35) -- turns at almost 20.05 degrees
      self:sleep(2000)
      
      -- first circle
      self.pegasus:set_relative_speed(100,175,0)
      self:sleep(3500)
      self.pegasus:stop()
  
      self:get_plant() 
  
      self.pegasus:set_relative_speed(0,0,1.8) 
      self:sleep(2000) 
  
      
      self.pegasus:set_relative_speed(0,192,0)
      self:sleep(4000)
      self.pegasus:stop()
  
      -- -goes to the gardener next to th private blue zone
      self:set_plant()
  
      self.pegasus:set_relative_speed(0,0,1.8) 
      self:sleep(2000) 
  
      self.pegasus:set_relative_speed(0,180,0) -- 2nd circle
      self:sleep(5000)
      self.pegasus:stop()
  
      self:get_plant() 
  
      self.pegasus:set_relative_speed(0,0,1.8) 
      self:sleep(2000) 
  
      self.pegasus:set_relative_speed(0,180,0) -- gardener
      self:sleep(5000)
      self.pegasus:stop()
  
      self:set_plant()
  
      -- va au panneau
       
       self.pegasus:set_relative_speed(-300,0,0) 
       self:sleep(5000)

       for j = 1, 2 do
        for i = 1, 3 do
            self.panneau:setTeam("B")
    
            self.pegasus:set_relative_speed(0.1,-225,0) 
            self:sleep(1000)
        end
        self.pegasus:set_relative_speed(0.1,-300,0)
        self:sleep(1000)
    end

    self.pegasus:set_relative_speed(0.1,355,0)
    self:sleep(6000)
     
    self.pegasus:stop()
    self.panneau:stop()

end 

function strat:onEnd ()

    self.pegasus:stop()
    self.panneau:initpos()

end
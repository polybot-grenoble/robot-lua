require "common_function"
strat.name = "Demo3" 
strat.team = 1          
strat.required_peripherals = {
    "pegasus", "plante", "panneau", "lidar"
}
--strat a test sur terrain (faite sur simulateur)
--deplace 3 zones de plantes avant de tourner les panneaux en rentrant
-- mettre le robot avec axe x et confondus avec ceux du terrain et en plein centre de la zone bleue reservee

cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.plante:initpos()
end 

function strat:main()

      self.pegasus:set_relative_speed(0,0,-1.658) 
      self:sleep(2000)
      
      -- first circle
      self.pegasus:set_relative_speed(94,191,0) -- (329, 668.5)mm
      self:sleep(3500)
      self.pegasus:stop()
  
      self.plante:open()
      self:sleep(500)
      self.plante:take() 
      self:sleep(1000)
      self.plante:close()
      self:sleep(1000) 
      self.plante:up()
      self:sleep(2000) 

        -- goes to the gardener on the top
      self.pegasus:set_relative_speed(0,-100,0) -- (0, -100)mm
      self:sleep(1000)
      self.pegasus:stop()

      self.pegasus:set_relative_speed(0,0,2.088) 
      self:sleep(2000) 
  
      self.pegasus:set_relative_speed(83.3,150,0) --(250, 450)mm
      self:sleep(3000)
      self.pegasus:stop()
  
      --drop the plants in the gardener
      self.plante:down()
      self:sleep(1000) 
      self.plante:open_slow()
      self:sleep(1000) 
      self.plante:up()
      self:sleep(500)
      self.plante:close()

      --SET : probably in the position (808,1900) with an angle of 5.888rad due to the collision with the edge
      self.pegasus:set_relative_speed(0,-100,0) -- 2nd circle (0,-400)mm
      self:sleep(4000)
      self.pegasus:stop()
  
      self.pegasus:set_relative_speed(0,0,-1.57) --after that the angle is -1.175rad in the reality
      self:sleep(2000)
  
      self.pegasus:set_relative_speed(83.3,150,0) --(250, 450)mm
      self:sleep(3000)
      self.pegasus:stop()
      --take the plants
      self.plante:open()
      self:sleep(500)
      self.plante:take() 
      self:sleep(1000)
      self.plante:close()
      self:sleep(1000) 
      self.plante:up()
      self:sleep(2000)

      self.pegasus:set_relative_speed(0,0,3.14) 
      self:sleep(2000)
      
      self.pegasus:set_relative_speed(133.3,188.3,0) --(800, 1130)mm
      self:sleep(6000)
      self.pegasus:stop()

        --slide to remove the pots and take position in front of the gardener
      self.pegasus:set_relative_speed(116.67,60,0) --(-350, 180)mm
      self:sleep(3000)
      self.pegasus:stop()
        --drop the plants
      self.plante:down()
      self:sleep(1000) 
      self.plante:open_slow()
      self:sleep(1000) 
      self.plante:up()
      self:sleep(500)
      self.plante:close()
    --the current datas (91,1381,-4.315)
    
    --let's take the circle in the bottom
      self.pegasus:set_relative_speed(-214.29,0,0) --(-750, 0)mm
      self:sleep(3500)
      self.pegasus:stop() 
      
      self.pegasus:set_relative_speed(0,0,-3.14)
      self:sleep(3000)
      self.pegasus:stop()

      self.pegasus:set_relative_speed(57.14,142.86,0) --(200, 500)mm
      self:sleep(3500)
      self.pegasus:stop()

      --take the plants
      self.plante:open()
      self:sleep(500)
      self.plante:take() 
      self:sleep(1000)
      self.plante:close()
      self:sleep(1000) 
      self.plante:up()
      self:sleep(2000)

      --currently (920,699,-1.172)

      self.pegasus:set_relative_speed(0,128.57,0) --(0, 1900)mm
      self:sleep(7000)
      self.pegasus:stop()

      --slide to remove the pots
      self.pegasus:set_relative_speed(170,-24,0) --(850, -120)mm
      self:sleep(5000)
      self.pegasus:stop()
      --drop the plants in the gardener
      self.plante:down()
      self:sleep(1000) 
      self.plante:open_slow()
      self:sleep(1000) 
      self.plante:up()
      self:sleep(500)
      self.plante:close()

      --it's time for solar panels
      self.pegasus:set_relative_speed(0,-200,0) --(0, -1000)mm
      self:sleep(5000)
      self.pegasus:stop()

      self.pegasus:set_relative_speed(0,0,2.74)
      self:sleep(5000)
      self.pegasus:stop()
    
      self.pegasus:set_relative_speed(-50,0,0) --(-100, 0)mm
      self:sleep(2000)
      self.pegasus:stop()

      --turn the solar panels and go to the final area
      self.panneau:setTeam("B")
      self.pegasus:set_relative_speed(9,125,0) --(30, 750)mm
      self:sleep(6000)
      self.pegasus:stop()
      --accelerate
      self.pegasus:set_relative_speed(15,225,0) --(30, 450)mm
      self:sleep(2000)
      self.pegasus:stop()
      --decelerate and arrive to the final position
      self.pegasus:set_relative_speed(6,110,0) --(30, 600)mm
      self:sleep(5500)
      self.pegasus:stop()

end 

function strat:onEnd ()

    self.pegasus:stop()
    self.panneau:reset()
    self.plante:initpos()

end
strat.name = "manual_udp"
strat.team = 1
strat.required_peripherals = {"gamepad", "panneau", "pegasus"}

strat.x = 0
strat.mult = 2

function strat:init ()
    self.pegasus:set_position(0, 0, 0)
    self.gamepad:updateData()
    self.panneau:stop()
end

function strat:main ()
    local run = true

    while run do
        self.gamepad:updateData()
        
        if self.x ~= self.gamepad.x then
            self.x = self.gamepad.x

            if self.x == 1 then
               self.panneau:setTeam("Y")
            else
               self.panneau:stop()
            end
        end

        local jlx = self.gamepad.leftJoystick[1]
        local jly = self.gamepad.leftJoystick[2]
        local jrx = self.gamepad.rightJoystick[1]
        
        self.pegasus:set_speed(100*self.mult*jlx, 100*self.mult*jly, self.mult*jrx)

        self:sleep(10)

    end
end

function strat:onEnd ()
    self.pegasus:stop()
    self.panneau:stop()
end
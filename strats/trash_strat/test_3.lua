require "common_function"
strat.name = "Test 3 Pegasus" 
strat.team = 1
strat.required_peripherals = {
     "pegasus", "lidar"
}

-- if "pegasus" does not response
cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.gamepad:updateData()
end 

function strat:main()
    self.pegasus:stop()
end 

function strat:onEnd ()

    self.pegasus:stop()

end

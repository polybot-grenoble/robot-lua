strat.name = "manual"
strat.team = 1
strat.required_peripherals = {"gamepad", "pegasus", "panneau"}

strat.cache = 0

function strat:init ()
    self.pegasus:set_position(0, 0, 0)
    self.gamepad:updateData()
    self.panneau:stop()
end

function strat:main ()
    local run = true

    while run do
        self.gamepad:updateData()
        self:sleep(100)
        -- local x = self.gamepad.x
        -- local b = self.gamepad.b
        -- local jlx = self.gamepad.leftJoystick[1]
        -- local jly = self.gamepad.leftJoystick[2]
        -- local jrx = self.gamepad.rightJoystick[1]
        
        -- self:sleep(10)
        -- self:log(""..x)

        -- if x == 1 then
        --     self.panneau:setTeam("Y")
        -- elseif b == 1 then
        --     self.panneau:setTeam("B")
        -- else
        --     self.panneau:stop()
        -- end

        -- self.pegasus:set_relative_speed(jlx, jly, jrx)
    end
end

function strat:onEnd ()
    self.pegasus:stop()
    self.panneau:stop()
end
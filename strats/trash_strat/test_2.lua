require "common_function"
strat.name = "Test 2 Plante" 
strat.team = 1
strat.required_peripherals = {
    "plante", "pegasus", "lidar"
}

-- if "panneau" does not work 
cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.plante:initpos()
end 

-- start in the private blue zone
function strat:main()

     self.pegasus:set_relative_speed(0,0,0.35) -- turn at almost 15 degres 
     self:sleep(2000)
     
     -- goes first circle
     self.pegasus:set_relative_speed(100,175,0)
     self:sleep(3500)
     self.pegasus:stop()
 
     self:get_plant()
 
     -- goes to the gardener next to the private blue zone
     self.pegasus:set_relative_speed(0,0,1.8) 
     self:sleep(2000) 
 
     
     self.pegasus:set_relative_speed(0,192,0)
     self:sleep(4000)
     self.pegasus:stop()
 
     self:set_plant()
 
     self.pegasus:set_relative_speed(0,0,1.8) 
     self:sleep(2000) 
 
     self.pegasus:set_relative_speed(0,180,0) --2nd circle
     self:sleep(5000)
     self.pegasus:stop()
 
     self:get_plant()
 
     self.pegasus:set_relative_speed(0,0,1.8) 
     self:sleep(2000) 
 
     self.pegasus:set_relative_speed(0,180,0) -- gardener
     self:sleep(5000)
     self.pegasus:stop()
 
     self:set_plant()
     
end 

function strat:onEnd ()

    
    self.pegasus:stop()
    self.plante:reset()
    self.plante:initpos()

end
require "common_function"
require "advanced_sleep"

strat.name = "rm" 
strat.team = 5         
strat.required_peripherals = {
    "pegasus", "lidar"
}

cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    -- self.plante:initpos()
end 

-- Test fiabilite advanced_sleep

function strat:main()

    local t0 = self:millis()
    local tt = t0
    local tt_1 = t0

    -- tourne les panneaux solaires
    advanced_sleep(self, self.pegasus, 1000)
    
    self.pegasus:set_relative_speed(-15,180,0)
    
    advanced_sleep(self, self.pegasus, 2000)
    tt = self:millis()
    self:log("t1: ".. (tt-tt_1))
    tt_1 = tt

    self.pegasus:set_relative_speed(180,-15,0)
    
    advanced_sleep(self, self.pegasus, 3000)
    tt = self:millis()
    self:log("t2: ".. (tt-tt_1))
    tt_1 = tt

    self.pegasus:set_relative_speed(-15,180,0)
    
    advanced_sleep(self, self.pegasus, 4000)
    tt = self:millis()
    self:log("t3: ".. (tt-tt_1))
    tt_1 = tt

    self.pegasus:set_relative_speed(180,-15,0)
    
    advanced_sleep(self, self.pegasus, 5000)
    tt = self:millis()
    self:log("t4: ".. (tt-tt_1))
    tt_1 = tt

    self.pegasus:stop()
    self.pegasus:unplug_motors()

    self:log("total: ".. (self:millis()-t0))
end 

function strat:onEnd()

    self.pegasus:unplug_motors()
    -- self.plante:initpos()

end

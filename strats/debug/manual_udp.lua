strat.name = "manual_udp"
strat.team = 1
strat.required_peripherals = {"gamepad", "panneau", "pegasus", "plante"}

strat.x = 0
strat.pince_high = 0
strat.pince_open = 0
strat.mult = 2

function strat:init ()
    self.pegasus:set_position(0, 0, 0)
    self.gamepad:updateData()
    self.panneau:stop()
end

function strat:main ()
    local run = true

    while run do
        self.gamepad:updateData()
        -- local x = self.gamepad.x
        if self.x ~= self.gamepad.x then
            self.x = self.gamepad.x
            -- self:log(""..self.x)

            if self.x == 1 then
               self.panneau:setTeam("Y")
            else
               self.panneau:stop()
            end
        end

        if self.pince_high ~= self.gamepad.crossY then
            self.pince_high = self.gamepad.crossY
            if self.pince_high == 1 then
                self.plante:up()
            elseif self.pince_high == -1 then
                self.plante:down()
            end--UwU--
        end

        if self.pince_open ~= self.gamepad.crossX then
            self.pince_open = self.gamepad.crossX
            if self.pince_open == 1 then
                self.plante:open()
            elseif self.pince_open == -1 then
                self.plante:close()
            end
        end

        local jlx = self.gamepad.leftJoystick[1]
        local jly = self.gamepad.leftJoystick[2]
        local jrx = self.gamepad.rightJoystick[1]
        
        self.pegasus:set_relative_speed(1000*self.mult*jlx, 1000*self.mult*jly, self.mult*jrx)

        self:sleep(10)
    end
end

function strat:onEnd ()
    self.pegasus:stop()
    self.panneau:stop()
end
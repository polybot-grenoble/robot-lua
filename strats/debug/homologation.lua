require "common_function"
require "advanced_sleep"

strat.name = "homologation" 
strat.team = 2         
strat.required_peripherals = {
    "pegasus", "panneau", "lidar"
}

cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    -- self.plante:initpos()
end 

-- strat pour homologation dynamique
--position de depart dans la zone en bas à droite en position turn the solar panels(jaune) (2856, 129, 4.712)

function strat:main()

    -- tourne les panneaux solaires
    advanced_sleep(self, self.pegasus, 300)
    -- for j = 1, 2 do
    --     for i = 1, 3 do
    --         self.panneau:setTeam("Y")
    
    --         self.pegasus:set_relative_speed(-5,225,0) 
    --         advanced_sleep(self, self.pegasus, 1000)
    --     end
    --     self.pegasus:set_relative_speed(-5,300,0)
    --     advanced_sleep(self, self.pegasus, 1000)
    -- end

      self.panneau:setTeam("Y")
      self.pegasus:set_relative_speed(-15,180,0) -- (-150, 1800)mm
      advanced_sleep(self, self.pegasus, 2000)
      -- retour bas en haut à gauche
      self.panneau:stop()
      self.pegasus:set_relative_speed(220,-180,0) -- (1650, -1800)mm (2349,2700)
      advanced_sleep(self, self.pegasus, 2500)
      self.pegasus:unplug_motors()
end 

function strat:onEnd()

    self.pegasus:unplug_motors()
    -- self.plante:initpos()

end

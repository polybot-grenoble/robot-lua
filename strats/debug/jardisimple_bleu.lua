require "common_function"

strat.name = "jardisimple_bleu" 
strat.team = 1        
strat.required_peripherals = {
    "pegasus", "plante", "panneau", "lidar"
}

-- étape 1 : depart en haut à gauche bleu (147, 1851, 1.536)
-- temps du match : -s (sans compter le temps des mouvements de l'actionneur plante)

--cruise speed : 100 mm/s
--delicate speed : 50 mm/s

cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.plante:initpos() --attention verif si c'est bien initpos
end 

function strat:main()
      
    advanced_sleep(self, self.pegasus, 300)

    --étape 2 :
    self.pegasus:set_relative_speed(341,-366,0) -- aller vers plantes
    advanced_sleep(self, self.pegasus, 1000) --cruise speed

    self:set_plant()

    --étape 3 : 
    self.pegasus:set_relative_speed(455,-154,0) -- rassembler plantes
    advanced_sleep(self, self.pegasus, 2000) --delicate speed

    self:get_plant()

    --étape 4 : 
    self.pegasus:set_relative_speed(0,-0, -4.354) -- rotation vers jardinière
    advanced_sleep(self, self.pegasus, 2000) --delicate speed    

    --étape 5 : 
    self.pegasus:set_relative_speed(-185, 568, 0) -- déplacer vers jardinière
    advanced_sleep(self, self.pegasus, 2000) --delicate speed    

    --étape 6 & 7 : 
    self:set_plant() --descendre et ouvrir pinces

    --étape 8 : 
    self.pegasus:set_relative_speed(-469, -1734, 0) -- déplacer vers jardinière
    advanced_sleep(self, self.pegasus, 3000) --delicate speed        

    self.pegasus:unplug_motors()

end
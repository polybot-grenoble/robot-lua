require "common_function"

strat.name = "demojaune" 
strat.team = 2          
strat.required_peripherals = {
    "pegasus", "plante", "panneau" --"lidar"
}

cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.plante:initpos()
end 

--position de depart (2866, 1844, 180)

function strat:main()

      self.pegasus:set_relative_speed(0,0,-1.658) 
      self:sleep(2000)
      
      -- first circle
      self.pegasus:set_relative_speed(138.3,83.3,0) -- (830, 500)mm
      self:sleep(6000)
      self.pegasus:stop()

      -- move back a little
      self.pegasus:set_relative_speed(-50,-80,0) -- (-25, -40)mm
      self:sleep(500)
      self.pegasus:stop()

      --take plantes
      self:get_plant() 

        -- goes to the gardener on the top
      self.pegasus:set_relative_speed(-50,-133.3,0) -- (-150, -400)mm
      self:sleep(3000)
      self.pegasus:stop()
      --currently (2211, 1784, 3.142)

      self.pegasus:set_relative_speed(0,0,-0.916) --(-2.747)
      self:sleep(3000)
  
      self.pegasus:set_relative_speed(65,100,0) --(65, 100)mm
      self:sleep(1000)
      self.pegasus:stop()
  
      --drop the plants in the gardener
      self:set_plant()

      --for the moment
      self.pegasus:set_relative_speed(-60,-240,0) -- 2nd circle (-300,-1200)mm
      self:sleep(5000)
      self.pegasus:stop()
  
      self.pegasus:set_relative_speed(0,0,0.524) -- (1.571)
      self:sleep(3000)
  
      self.pegasus:set_relative_speed(50,100,0) --(150, 300)mm
      self:sleep(3000)
      self.pegasus:stop()
        -- move back a little
      self.pegasus:set_relative_speed(-50,-80,0) -- (-25, -40)mm
      self:sleep(500)
      self.pegasus:stop()

      --take the plants
      self:get_plant()
      -- currently(2081,701,4.318)
      
      self.pegasus:set_relative_speed(0,0,-1.309) --(-2.618)
      self:sleep(2000)
      
      self.pegasus:set_relative_speed(16.67,183.3,0) --(-100, 1100)mm
      self:sleep(6000)
      self.pegasus:stop()

        --let's turn the solar panel
      self.pegasus:set_relative_speed(0,-166.67,0) --(0, -1000)mm
      self:sleep(6000)
      self.pegasus:stop()

      self.pegasus:set_relative_speed(0,0,1.112) --(2.224)
      self:sleep(2000)
      --the robot is correctly oriented to turn the solar panels

      self.pegasus:set_relative_speed(-130,180,0) --(-650, 900)mm
      self:sleep(5000)
      self.pegasus:stop()

      --turn the solar panels and go to the final area
      self.panneau:setTeam("Y")
      self.pegasus:set_relative_speed(9,125,0) --(30, 750)mm
      self:sleep(6000)
      self.pegasus:stop()
      --accelerate
      self.pegasus:set_relative_speed(15,225,0) --(30, 450)mm
      self:sleep(2000)
      self.pegasus:stop()
      --decelerate and arrive to the final position
      self.pegasus:set_relative_speed(6,110,0) --(30, 600)mm
      self:sleep(5500)
      self.pegasus:stop()
    
end 

function strat:onEnd ()

    self.pegasus:stop()
    self.panneau:stop()
    self.plante:initpos()

end
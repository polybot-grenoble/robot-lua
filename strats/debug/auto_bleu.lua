require "auto_speed"

strat.name = "auto_bleu"
strat.team = 1
strat.required_peripherals = {
    "pegasus", "panneau", "lidar"
}

setup_auto_speed(strat)

function strat:init ()

    self.linear_speed = 50 -- mm/s
    self:set_current_zone(101)

end

function strat:main ()

    self:travel_sleep(100)

    self:go_to_zone(105)

end

function strat:onEnd ()

    self.pegasus:stop()
    self.pegasus:unplug_motors()
    
    self.panneau:stop()

end
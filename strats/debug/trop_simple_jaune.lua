require "common_function"

strat.name = "trop_simple_jaune" 
strat.team = 2          
strat.required_peripherals = {
    "pegasus", "plante", "panneau", "lidar"
}

--depart en haut à droite jaune (2860, 1849, 3.142)
-- temps du match :  (sans compter le temps des mouvements de l'actionneur plante)
-- objectif : Aller prendre les plantes les plus proches et aller dans la zone centrale en face

cree_common (strat)

function strat:init()
    self.pegasus:set_position(2860, 1849, 3.142)
    self.plante:initpos() --attention verif si c'est bien initpos
end 

function strat:main()
      
    advanced_sleep(self, self.pegasus, 300)

    self.pegasus:set_relative_speed(100, 100, 0) -- (100,100)mm 
    advanced_sleep(self, self.pegasus, 1000) -- 5s

    self.pegasus:set_relative_speed(0,0,-0.611) -- On tourne de 35° vers la droite
    advanced_sleep(self, self.pegasus, 1000)

    self.pegasus:set_relative_speed(70, 160, 0) -- (350,800)mm 
    advanced_sleep(self, self.pegasus, 5000) -- 5s

    self.pegasus:stop()

    --take the plants
    self:get_plant() 

    self.pegasus:set_relative_speed(0, 0, -0.4175) -- ~50° (0.835 rad)
    advanced_sleep(self, self.pegasus, 2000) -- 2s

    self.pegasus:set_relative_speed(0, 170 , 0) -- (0,1700)mm 
    advanced_sleep(self, self.pegasus, 10000) -- 10s

    self.pegasus:stop() 

    --drop the plants in the init area
    self:set_plant()
    self.pegasus:unplug_motors()
end

function strat:onEnd ()
    self.pegasus:unplug_motors()
    self.panneau:stop()

end

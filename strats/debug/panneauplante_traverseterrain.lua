require "common_function"

strat.name = "panneau_traverseterrain" 
strat.team = 1          
strat.required_peripherals = {
    "pegasus", "plante", "panneau", "lidar"
}

-- depart en bas à droite bleu  (133, 141, 0)
-- temps du match : 48s (acctionneur panneau solaire)

cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.plante:initpos() --attention verif si c'est bien initpos
end 

function strat:main()
      
    advanced_sleep(self, self.pegasus, 300)
    
    self.pegasus:set_relative_speed(25,50,0) -- (50, 100)mm
    advanced_sleep(self, self.pegasus, 2000)
    
    self.panneau:stop()
    self.pegasus:set_relative_speed(0,0,1.571) -- (-1.571)
    advanced_sleep(self, self.pegasus, 1000)
      
    self.pegasus:set_relative_speed(-100,0,0) -- (-120,0)mm
    advanced_sleep(self, self.pegasus, 1200) --deplacement de 1,2 sec
     
    self.panneau:setTeam("B")
    self.pegasus:set_relative_speed(-15,-200,0) -- (0, -600)mm
    advanced_sleep(self, self.pegasus, 300)
    self.panneau:stop()

    -- aller à la base + plantes
    self.pegasus:set_relative_speed(0,200,0) -- (0, 100)mm
    advanced_sleep(self, self.pegasus, 500)
        -- on tourne pour avoir l'actionneur plante devant
        self.pegasus:set_relative_speed(0,0,1.1) -- (2.2)
        advanced_sleep(self, self.pegasus, 2000)
    
        -- on ouvre l'actionneur plante
        self.plante:open()
        self:sleep(500)
        self.plante:down()
        self:sleep(1000) 

        -- aller à la base
        self.pegasus:set_relative_speed(110,180,0) -- (1100, 1800)mm
        advanced_sleep(self, self.pegasus, 10000)

    -- fin 
    self.pegasus:stop()
    self.pegasus:unplug_motors()
end 

function strat:onEnd ()
    self.pegasus:unplug_motors()
    self.panneau:stop()

end
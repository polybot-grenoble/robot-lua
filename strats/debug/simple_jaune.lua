require "common_function"

strat.name = "simple_jaune" 
strat.team = 2        
strat.required_peripherals = {
    "pegasus", "panneau", "lidar"--"plante",
}

--depart en bas à droite jaune (2860, 136, 4.712)
-- temps du match : 48s (sans compter le temps des mouvements de l'actionneur plante)

cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    --self.plante:initpos() --attention verif si c'est bien initpos
end 

function strat:main()

    advanced_sleep(self, self.pegasus, 300)
      -- turn les three solar panels
      self.panneau:setTeam("Y")
    
    self.pegasus:set_relative_speed(-30,400,0) -- (0, 1000)mm 
    advanced_sleep(self, self.pegasus, 2700)
    self.panneau:stop()

    -- go to the first circle
    self.pegasus:set_relative_speed(250,0,0) -- (300,0 )mm      change en 300
    advanced_sleep(self, self.pegasus, 2000)


    -- rentre à la base
    self.pegasus:set_relative_speed(0,0,-1.512)     -- rotation de 100 deg = 4.538
    advanced_sleep(self, self.pegasus, 3000)

    self.pegasus:set_relative_speed(71.4,200,0)    --(500,1400)
    advanced_sleep(self, self.pegasus, 7000)
    self.pegasus:unplug_motors()


      
    -- advanced_sleep(self, self.pegasus, 300)
    --   -- turn les three solar panels
    --   self.panneau:setTeam("Y")
    
    -- self.pegasus:set_relative_speed(-30,400,0) -- (0, 600)mm    change a 1000
    -- advanced_sleep(self, self.pegasus, 2700)
    -- self.panneau:stop()


    
    --   -- go to the first circle
    --   self.pegasus:set_relative_speed(250,0,0) -- (500,0 )mm      change en 300
    --   advanced_sleep(self, self.pegasus, 3000)

    --   self.pegasus:set_relative_speed(0,150,0) -- (0,100)mm
    --   advanced_sleep(self, self.pegasus, 1000)
    --   --self.pegasus:stop()
    --   --self.get_plant()                          -- current position (2159,635,4.712)

    --   self.pegasus:set_relative_speed(0,0,1.3965)     -- rotation de -200 deg = -2.793
    --   advanced_sleep(self, self.pegasus, 2000)
    --   self.pegasus:set_relative_speed(43.33,260,0)    --(100,600)  current pos (2698,372,2.024)
    --   advanced_sleep(self, self.pegasus, 3000)
    --   --self.pegasus:stop()
    --   --self:set_plant()

    --   self.pegasus:set_relative_speed(-260,-55.71,0)    --(-1400,-300)
    --   advanced_sleep(self, self.pegasus, 7000)
    --   self.pegasus:unplug_motors()
end 

function strat:onEnd ()
    self.pegasus:unplug_motors()
    self.panneau:stop()

end
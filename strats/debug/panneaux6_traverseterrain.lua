require "common_function"

strat.name = "rtv6"
strat.team = 1          
strat.required_peripherals = {
    "pegasus", "panneau", "lidar"
}

-- depart en bas à droite bleu  (133, 141, 0)
-- temps du match : 48s (acctionneur panneau solaire)

cree_common (strat)

function strat:init()
    self.pegasus:set_position(133, 141, 0)
    -- self.plante:initpos() --attention verif si c'est bien initpos
end 

function strat:main()
      
    advanced_sleep(self, self.pegasus, 300)
    
    self.pegasus:set_relative_speed(25,50,0) -- (50, 100)mm
    advanced_sleep(self, self.pegasus, 2000)
    
    self.panneau:stop()
    self.pegasus:set_relative_speed(0,0,0.524) -- (-1.571)
    advanced_sleep(self, self.pegasus, 3000)
      
    self.pegasus:set_relative_speed(-100,0,0) -- (-120,0)mm
    advanced_sleep(self, self.pegasus, 2000) --deplacement de 1,2 sec
     
    self.panneau:setTeam("B")
    self.pegasus:set_relative_speed(-11,-200,0) -- (0, -600)mm
    advanced_sleep(self, self.pegasus, 11500)
    self.panneau:stop()

    -- -- aller à la base
    self.pegasus:set_relative_speed(400,360,0) -- (800, -1960)mm
    advanced_sleep(self, self.pegasus, 5000)
    
    -- fin 
    self.pegasus:stop()
    self.pegasus:unplug_motors()
end 

function strat:onEnd ()
    self.pegasus:unplug_motors()
    self.panneau:stop()

end
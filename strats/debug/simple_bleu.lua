require "common_function"

strat.name = "simple_bleu" 
strat.team = 1         
strat.required_peripherals = {
    "pegasus", "plante", "panneau", "lidar"
}

--depart en bas à droite jaune (133, 141, 0)

cree_common (strat)

function strat:init()
    self.pegasus:set_position(0, 0, 0)
    self.plante:initpos()
end 


function strat:main()

      advanced_sleep(self, self.pegasus, 300)
      
      -- turn les three solar panels
      
      self.pegasus:set_relative_speed(25,50,0) -- (50, 100)mm
      advanced_sleep(self, self.pegasus, 2000)
    
      self.pegasus:set_relative_speed(0,0,1.571) -- (-1.571)
      advanced_sleep(self, self.pegasus, 1000)
      
      self.pegasus:set_relative_speed(-100,0,0) -- (-120,0)mm
      advanced_sleep(self, self.pegasus, 1200) --deplacement de 1,2 sec
     
      self.panneau:setTeam("B")
      self.pegasus:set_relative_speed(-15,-250,0) -- (0, -600)mm
      advanced_sleep(self, self.pegasus, 3000)
      self.panneau:stop()
    
      self.pegasus:set_relative_speed(200,0,0) -- (200, 0)mm
      advanced_sleep(self, self.pegasus, 1000)

      self.pegasus:set_relative_speed(0,0,4.712) -- (1.571)
      advanced_sleep(self, self.pegasus, 1000)
      
      
      self.pegasus:set_relative_speed(75,125,0) -- (125, 350)mm
      advanced_sleep(self, self.pegasus, 2000)
      self.pegasus:stop()
      self:get_plant() 
     
      --currently (966, 654, 0.174)
      --take the plants

      self.pegasus:set_relative_speed(0,0,2.27) -- (3.491)
      advanced_sleep(self, self.pegasus, 2000)
      
      self.pegasus:set_relative_speed(133,200,0) -- (400, 600)mm
      advanced_sleep(self, self.pegasus, 3000)
      self.pegasus:stop() 
      self:set_plant()
      --currently (319, 334, 3.665)
      --drop the plants in the init area
      

      self.pegasus:set_relative_speed(0,0,2.443) -- (2.792)
      advanced_sleep(self, self.pegasus, 2000)
      self.pegasus:stop()
     
      self:get_plant()
      self.pegasus:set_relative_speed(112.5,237,5,0) -- (450,950)mm
      advanced_sleep(self, self.pegasus, 4000)

      --currently (926, 1201, 0.174)
      --take the plants

      self.pegasus:set_relative_speed(0,0,1.571) -- (-1.571)
      advanced_sleep(self, self.pegasus, 1000)
      
      self.pegasus:set_relative_speed(166,200,0) -- (500,600)mm
      advanced_sleep(self, self.pegasus, 3000)
      self.pegasus:stop()

      self:set_plant()
      self.pegasus:unplug_motors()
end 

function strat:onEnd ()
    self.pegasus:unplug_motors()
    self.panneau:stop()

end
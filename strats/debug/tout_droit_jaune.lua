require "common_function"

strat.name = "tout_droit_jaune" 
strat.team = 2          
strat.required_peripherals = {
    "pegasus", "plante", "panneau", "lidar"
}

--depart en haut à droite jaune (2860, 1849, 3.142)
-- temps du match :  (sans compter le temps des mouvements de l'actionneur plante)
-- objectif : Aller tout droit d'une zone à l'autre

cree_common (strat)

function strat:init()
    self.pegasus:set_position(2860, 1849, 3.142)
    self.plante:initpos() --attention verif si c'est bien initpos
end 

function strat:main()
      
    advanced_sleep(self, self.pegasus, 300)

    self.pegasus:set_relative_speed(0, 190, 0) -- (0,1600)mm 
    advanced_sleep(self, self.pegasus, 10000) -- 10s

    self.pegasus:stop()

    self.pegasus:unplug_motors()
end

function strat:onEnd ()
    self.pegasus:unplug_motors()
    self.panneau:stop()

end


-- Pas d'acces aux variables globales depuis une librairie.
-- Il faut passer par les parametres.

function goto_fake (strat,pegasus,x, y,rot, speed,rot_speed)
    local pos = pegasus:get_position()
    local distance_x_plat=x-pos[1] --x
    local distance_y_plat=y-pos[2] --y  dans le repere du plat
    --convertion du vecteur dans le repere du robot (prise en compte de la rotation actuel du robot)
    local distance_x= cos(pos[3])*distance_x_plat+sin(pos[3])*distance_y_plat --pas sûr de la synthaxe on doit peut-être définir sin=math.sin et pareil pour cos
    local distance_y=cos(pos[3])*distance_y_plat-sin(pos[3])*distance_x_plat
    --gestion des signes
    local sign_x=1
    local sign_y=1
    local rot_sign=1
    if rot<0 then
        rot_sign=-1
    end
    if distance_x<0 then 
        sign_x=-1
    end
    if distance_y<0 then
        sign_y=-1
    end
    --convertion en relative speed
    if distance_x*sign_x>distance_y*sign_y then
        pegasus:set_relative_speed(speed*sign_x,speed*(distance_x*sign_x/distance_y),0)
        strat:advanced_sleep(strat,pegasus,distance_x*sign_x/speed)
        pegasus:set_relative_speed(0,0,rot_speed*rot_sign)
        strat:advanced_sleep(strat,pegasus,rot/rot_speed*rot_sign)
    else 
        pegasus:set_relative_speed(speed*(distance_y*sign_y/distance_x),speed*sign_y,0)
        strat:advanced_sleep(strat,pegasus,distance_y*sign_y/speed)
        pegasus:set_relative_speed(0,0,rot_speed*rot_sign)
        strat:advanced_sleep(strat,pegasus,rot/rot_speed*rot_sign)
    end
end
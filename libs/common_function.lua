function cree_common (strat)

    function strat:get_plant()
        self.plante:open()
        self:sleep(500)
        self.plante:take() 
        self:sleep(1000)
        self.plante:close()
        self:sleep(1000) 
        self.plante:up()
        self:sleep(2000) 
    end

    function strat:set_plant()
        self.plante:down()
        self:sleep(1000) 
        self.plante:open_slow()
        self:sleep(1000) 
        self.plante:up()
        self:sleep(500)
        self.plante:close()
    end

    function strat:reset_position()
        self.pegasus:stop_with_response()
        local x, y, theta = self.superviseur:refresh_coordinate()
        self.pegasus:set_position(x, y, theta)
    end

    function strat:get_plant_number()
        self.pegasus:stop_with_response()
        return self.superviseur:refresh_coordinate()
    end

end
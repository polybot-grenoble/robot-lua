
-- Pas d'acces aux variables globales depuis une librairie.
-- Il faut passer par les parametres.

-- function advanced_sleep (strat, pegasus, duration)
    
--     while duration > 0 do
--         strat:sleep(10) -- ms
--         if pegasus:is_stopped() == 0 then -- if not stopped.
--             duration = duration - 10            
--         end
--     end
-- end

function advanced_sleep (strat, pegasus, duration)
    local t0 = strat:millis()
    while (strat:millis() - t0) < duration do
        local t1 = strat:millis()
        strat:sleep(250) -- ms
        if pegasus:is_stopped() == 1 then -- if stopped.
            local t2 = strat:millis()
            duration = duration + (t2 - t1) -- Take into account the sleep and is_stopped() duration.
        end
    end
end
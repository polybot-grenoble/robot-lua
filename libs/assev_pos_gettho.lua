-- Asservissement en posistion géré par Core en polling
-- A besoin du lidar et de la base mobile

function create_asserv (strat, lidar, pegasus)

    function fake_goto (x, y, speed)

        lidar:please_goto (x, y)

        local pos = pegasus:get_position()
        local eps = 10

        while ((math.abs(x - pos[1]) > eps) and (math.abs(y - pos[2]) > eps)) do
            
            local x_dir, y_dir = lidar:next_direction()

            if x_dir == nil then
                self:log("Failed to retrieve pos, quitting goto")
                break
            end

            local x_speed = x_dir * speed
            local y_speed = y_dir * speed

            pegasus:set_relative_speed(x_speed, y_speed, 0)

            strat:sleep(20) -- ms
            
            pos = pegasus:get_position()

        end

        pegasus:stop()

    end

    return {
        fake_goto
    }

end
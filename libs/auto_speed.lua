require "advanced_sleep"

function setup_auto_speed (strat)

    strat.linear_speed      = 50 -- mm/s
    
    function strat:travel_sleep (ms)
        advanced_sleep(self, self.pegasus, ms)
    end

    -- Tries to bring the robot to the desired coordinates
    function strat:go_to_target (x, y, theta)

        local pegasus = self.pegasus
        local curr_pos = pegasus:get_position()

        if curr_pos == nil then
            self:log("Failed to retreive the current position")
            return false
        end

        local dx = x - curr_pos[0]
        local dy = y - curr_pos[1]
        local dtheta = theta - curr_pos[2]

        self:log("Planned to travel a relative distance:\n - dx = " .. dx .. "mm\n - dy = " .. dy .. "mm\n - dtetha = " .. dtetha .. "rad")

        local distance = math.sqrt(dx * dx + dy * dy)
        if math.abs(distance) < 1 then
            self:log("Distance too small")
            return false
        end

        local travel_time = self.linear_speed / distance -- s
        self:log("Estimated travel time : " .. travel_sleep .. "s")

        local x_speed = (dx / distance) * self.linear_speed -- mm/s
        local y_speed = (dy / distance) * self.linear_speed -- mm/s
        local t_speed = dtheta / travel_time -- rad/s

        local sleep_time = math.floor(1000 * travel_time) + 1
        self:log("Resulting speeds:\n - x' = " .. x_speed .. "mm/s\n - y' = " .. y_speed .. "mm/s\n - dtetha = " .. t_speed .. "rad/s")

        local t0 = self:millis()
        pegasus:set_speed(x_speed, y_speed, t_speed)
        self:travel_sleep(sleep_time)
        local t1 = self:millis()

        pegasus:stop()

        local t = t1 - t0
        self:log("Travel took " .. t .. "ms, error = " .. (t - travel_time))

        return true

    end

    -- Positions de départ du robot par zone
    strat.zones_depart = {
        [101] = { x = 130,  y = 150,  theta = 0     },
        [102] = { x = 130,  y = 1000, theta = 0     },
        [103] = { x = 150,  y = 1860, theta = 1.571 },
        [104] = { x = 2850, y = 130,  theta = 4.712 },
        [105] = { x = 2860, y = 1000, theta = 3.142 },
        [106] = { x = 2860, y = 1850, theta = 3.142 },
    }

    -- Positions d'arrivée du robot par zone
    strat.zones_arrive = {
        [101] = { x = 215,  y = 215  },
        [102] = { x = 215,  y = 990  },
        [103] = { x = 215,  y = 1775 },
        [104] = { x = 2790, y = 215  },
        [105] = { x = 2790, y = 990  },
        [106] = { x = 2790, y = 1775 },
    }

    -- Définit la zone de départ du robot et sa rotation initiale par rapport à l'origine
    function strat:set_current_zone (zone)

        if zone < 101 or zone > 106 then
            self:log("Invalid zone " .. zone)
            return 
        end

        local z = self.zones_depart[zone]
        self.pegasus:set_position (z.x, z.y, z.theta)

        self:log("Set the robot position to { x: " .. z.x .. ", y: " .. z.y .. ", theta: " .. z.theta .. "}")

    end

    -- Envoie le robot dans une autre zone, avec une orientation définie
    function strat:go_to_zone (zone, theta)

        if zone < 101 or zone > 106 then
            self:log("Invalid zone " .. zone)
            return 
        end

        local z = self.zones_arrive[zone]
        self:log("Set the robot target to { x: " .. z.x .. ", y: " .. z.y .. ", theta: " .. theta .. "}")

        self:go_to_target(z.x, z.y, theta)

    end

end
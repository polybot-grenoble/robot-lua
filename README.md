# Robot Lua
In 2024, Polybot Grenoble developed an innovative system to control the entire robot logic and provides generic libraries to our developers. This system is called [Core](https://gitlab.com/polybot-grenoble/core). To sum up our ecosystem, a Raspberry Pi 4 (RPI4) and STM32L4 devices are connected together using a CAN network. Each STM32 is considered a peripheral and can map functions that can be called by the RPI4 and the other peripherals.

Example peripherals are :
- Wheels and motors
- LiDAR
- Actuators
- And so on...

This repo lists all the LUA peripherals and strategies used to control the robot's behaviour. If Core if reused over the years, please create a new branch specific to your year so we can keep track of the entire development.

GLHF.